Repo Test
#Repositorio de prueba Netcentric

##Configuración
git config --list, muestra la configuración global
git config propiedad "valor" (guarda en la propiedad en la configuración del proyecto)
git config --global user.name "me"
git config --global user.email "me@email.com"
git config --global color.ui true, muestra colores en el editor
git config --global core.editor sublime-text, cambia el editor por defecto a sublime-text

##Comandos básicos
git  -h, muestra las opciones del comando indicado
git init, inicializa un nuevo proyecto
git status, obtener información sobre el estado actual del repositorio
git clone, obtener un repositorio remoto
git add,  para añadir ficheros añadidos o modificados a la zona de staging/index
git commit -am "message", comitea los ficheros trakeados de una vez, suma de add + commit.
git commit -m "message" , solo hace commit del fichero indicado
git commit --amend, los cambios se añaden al último commit
git log,  muestra el histórico de commits
git log --oneline, cambia el formato del log
git show, muestra info del commit

##Alias
git config --unset alias.aliasname, elimina un alias
git config alias.co commit, crea el alias "co" parar hacer commit
git config alias.ch checkout, crea el alias "ch" parar hacer checkout

##Otros comandos
git rm --cached,  para mover de la zona de staging/index cuando NO HAY COMMITs todavía
git reset HEAD,  para mover de la zona de staging/index cuando HAY COMMITS
git rm,  para añadir el fichero borrado a la zona de staging/index

git stash, sólo funciona con ficheros que están en la zona de staging/index
git stash list, para ver todos los commits que hay en la zona de stash
git stash apply, recupera el último commit de la zona de stash
git stash apply <identificador, stash@{n}>, recupera el commit indicado por el identificador
git stash save "mensaje", guarda en la zona de stash con un mensaje personalizado
git stash pop, recupera el commit y lo borra de la zona de stash
git stash drop , elimina el commit de la zona de stash indicado por el identificador
git stash clear, borra toda la zona de stash
git show <id_stash>, muestra la información básica sobre el commit realizado en la zona de stash
git log --oneline -- , busca todos los commits que contengan un fichero

git diff -w master origin -- , compara un fichero entre dos ramas
git log master~12..master~10, muestra los commits que van del doceavo al décimo
git log --grep='reg-exp', obtiene los commits que coinciden con la expresión regular 'reg-exp'
git log --no-merges, mustra todos los commits salvo los que son de mergeo
git log [--since|--before|--after]={2014-04-18}, muestra los commits desde, anteriores o posteriores a la fecha indicada
git log --abbrev-commit, muestra el ID de commit abreviados
git log --stat, muestra los ficheros cambiados en cada commit
git --decorate --graph --oneline --all, --all incluye las ramas en el gráfico


##Deshacer cambios
git checkout -- , descarta los cambios realizados en la zona de trabajo y recupera los del último commit
git checkout <revision|nombre de la rama>, mueve el puntero a la revisión o rama indicada
git checkout master^, vuelve al commit anterior
git checkout master^^, vuelve al penúltimo commit
git checkout master~2, vuelve al penúltimo commit

##Deshacer commits
git reset --hard , elimina los cambios posteriores al commit indicado
git reset --soft , descarta los commits posteriores al commit indicado pero mueve los cambios a la zona de stagging/index
git reset --mixed , descarta los commits posteriores al commit indicado pero mueve los cambios a la zona de working

git gc --force, purga los commits eliminados
git revert , crea un nuevo commit con lo contrario del commit especificado
git branch -a, mustra todas las ramas, incluso las remotas
git branch -d , borra ramas que ya han sido mergeadas
git branch -D , borra ramas aunque no hayan sido mergeadas
git push  :, borrar una rama remota
git branch  /, me crea una rama trayéndome lo que hay en la rama remota
git checkout -b  /, me crea una rama trayéndome lo que hay en la rama remota y me posiciona
git reset --hard orig_head, hace el reset y vuelve al commit donde anteriormente estaba HEAD, sea en la rama que sea.

##Ramas
git rebase master, añade los commits de master a mi rama
git rebase -continue, continua con el rebase si ha dado conflictos

##Mergeos
git merge rama1 --no-ff, si hay posibilidad de hacer Fast Forward, no lo hace y crea un commit de mergeo
git config branch.mergeoptions "--no-ff", configuramos a nivel de proyecto que al mergear cualquier rama no se haga un Fast Forward.

##Remotos
git remote add  <url_repo>, enlazamos con un repo remoto distinto a origin (se suele llamar upstream)
git pull upstream master, nos traemos el código del repo remoto al que hemos enlazado
git fetch origin master, te trae los cambios de la rama master de remoto y los guarda en origin/master, pero NO los mergea
git push -f  , machacas lo que hay en el remoto con mi local

##Tags
git tag -a <tag_name> -m "message", Crear un tag anotado
git tag <tag_name>, Crear un tag ligero
git push  --tags, sube todos los tags al remoto

##Comparaciones
git diff , me muestra las diferencias entre mi área de trabajo y un commit
git diff --cached , me muestra las diferencias entre la zona index/stagging y un commit
git diff  , saca las diferencias entre dos commits
git diff .., saca las diferencias entre dos commits
git diff   , saca las diferencias entre dos commits de un fichero
git diff .. , saca las diferencias entre dos commits de un fichero

##Alterando commits
git checkout  && git cherry-pick master^2, compia el penúltimo commit de master a mi rama
git pull --rebase  
